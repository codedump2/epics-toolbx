EPICS Toolbox Base
==================

[![pipeline status](https://codebase.helmholtz.cloud/florin.boariu/epics-toolbx/badges/master/pipeline.svg)](https://codebase.helmholtz.cloud/florin.boariu/epics-toolbx/-/commits/master)
[![Latest Release](https://codebase.helmholtz.cloud/florin.boariu/epics-toolbx/-/badges/release.svg)](https://codebase.helmholtz.cloud/florin.boariu/epics-toolbx/-/releases)

This is a base image for a [Mutable Container Toolbox](https://containertoolbx.org/) containing
recent releases of the [EPICS](https://epics-controls.org/) beamline control system.
You can use it either as a base for EPICS-based tools and services, or as a command-line
toolbox for rapid access to the EPICS ecosystem, including commands like `caget` and
`camonitor`.

What's in the box
-----------------

### A comprehensige EPICS installation

In the end, the it's the [local Dockerfile](./Dockerfile) that's the authority
on this. But essentially, you get the following EPICS modules. The `/tmp/...`
folder for each of the packages indicates where the Dockerfile is instructed
to look for the sources while building the image. See the
[instructions for bulding from scratch](#building-from-scratch) to understand
how they will be used by default, or how to harness these directory
specifications in order to customize your build:
 
  - [BASE](https://github.com/epics-base/epics-base.git), version R7.0.6.1 from `/tmp/epics-base`
  - [SNCSEQ](https://github.com/mdavidsaver/sequencer-mirror), version R2-2-9 from `/tmp/sncseq`
  - [SSCAN](https://github.com/epics-modules/sscan.git), current HEAD from `/tmp/sscan`
  - [CALC](https://github.com/epics-modules/calc.git), current HEAD from `/tmp/calc`
  - [ASYN](https://github.com/epics-modules/asyn.git), version R4-42 from `/tmp/asyn`
  - [AUTOSAVE](https://github.com/epics-modules/autosave.git), current HEAD from `/tmp/autosave`
  - [BUSY](https://github.com/epics-modules/busy.git), current HEAD from `/tmp/busy`
  - [DEVIOCSTATS](https://github.com/epics-modules/iocStats.git), current HEAD `/tmp/iocStats`

Also a number of Linux packages and tools are installed, mostly needed for
building EPICS:

  - Development tools and libraries (gcc, autotools, ...)
  - Data format libaries (HDF5, zlilb, libjpeg, libxml2, ...)
  - GraphicsMagick
  - OpenCV
  - some X11 libs (X11 and Xext)

Epics is installed in `/opt/prefix` and the sources are, intentionally, deleted
after installing. Only the components that get specifically installed by the
EPICS module build system itself persist. (This means: watch out for EPICS modules
with broken install routines!)

### Bonus package `areaDetector`


Initially this begun as an image to run the
[areaDetector](https://github.com/areaDetector/areaDetector) IOC for X-ray detectors,
this one is also included, during build time expected in `/tmp/areaDetector`.
The following devices are enabled:

  - Pilatus
  - ffmpeg server
  - ffmpeg viewer
  - ADSimDetector
  - ADCSimDetector

This setup, however, is untested. Consider this a bonus package, an artefact of
the development process (if you will), which should probably be removed in the
future and placed in its own image with a wider variety of supported devices
(possibly based on `epics-toolbx`).

Installation and usage
----------------------

### Using pre-built images

The recommended stand-alone usage scenario if you have Podman and
[Toolbx](https://containertoolbx.org/) is retrieving a pre-built image from the
Helmholtz registry and basing a local toolbox off it:
```
    $ podman pull registry.hzdr.de/florin.boariu/epics-toolbx:latest
    $ toolbox create epics -i epics-toolbx
    $ toolbox enter epics
    ...
```

Alternatively, if you aren't using Toolbx, you can just run a shell within the
image:
```
   $ podman run -ti --rm registry.hzdr.de/florin.boariu/epics-toolbx:latest /bin/bash
```

### Building from scratch


If you can't or won't use the pre-built image from Helmholtz (sometimes they're out of
date), you can build the base image yourself. You can deal with dependencies
in two ways: (1) letting `Dockerfile` use the builtin defaults, or (2) overriding
source trees or versions yourself.

For both you need to download a copy of the `epics-toolb` "sources" (i.e.  the `Dockerfile`
definition) first. You can choose either URL:

  - [EPICS-Toolbx at Gitlab.com](https://gitlab.com/codedump2/epics-toolbx):
    ```
	   $ git clone https://gitlab.com/codedump2/epics-toolbx
	```
  - [EPICS-Toolbx from the Helmholtz Codebase](https://codebase.helmholtz.cloud/florin.boariu/epics-toolbx)
    ```
	   $ git clone https://codebase.helmholtz.cloud/florin.boariu/epics-toolbx
	```
	
In theory, both repositories should contain the same HEAD version; in practice,
sometimes they don't (...and sometimes even for good reason, e.g. as in recently when
the Helmholtz center suffered a breach and Git repository couldn't be updated for
months). So try which you think fits best. If you believe the HEAD version from
the repo you're about to pull is too old, try the other one.

#### Building with `Dockerfile` defaults

The simple variant goes like this (assuming we're going to pull from Gitlab.com):

```
    $ git clone https://gitlab.com/codedump2/epics-toolbox.
	$ podman build -t epics-toolbx -f epics-toolbx/Dockerfile
```

This will yield a local `localhost/epics-toolbx:latest` base image you can later
use with `toolbox create ...`, as indicated [above](#using-pre-built-images).
The `Dockerfile` will instruct `podman` to pull the necessary sources from
their repositories, encoded within the `Dockerfile`.

#### Building with custom sources and versions

Sometimes you might want more control over where the sources of various
EPICS components are retrieved from, or which versions. For instance: you
might want to use a local copy of the sources, or a different version.

The `Dockerfile` expects each of the EPICS packages in a subfolder in `/tmp/...`
(for instance: `/tmp/epics-base`) for the base package.
See [above](#what-s-in-the-box) for the expected folder of every package.
When building each package, the contents of the respective are checked.
If the folder is non-empty, it's expected to contain usable sources for the
respective package, and the build continues with whatever those contents are.
On the other hand, if the folder is non-existent or empty, a set of sources
is pulled from a default location. The URL and version are imposed as specified
in the [EPICS module list above](#what-s-in-the-box).

You can finetune control over which versions to actually pull by overriding
the environment variable `AD_EPICS_<package>_VERSION`.
For instance, setting `AD_EPICS_SNCSEQ_VERSION="R2-2-7"` will use version
2.2.7 of the SNC Sequencer instead of the default of 2.2.9.

For instance, the follwing code snipped will customize the build as follows:
  - download a copy of EPICS base into a local folder "`/home/user/my-epics`",
  - let you modify the sources as you see fit,
  - override version of the `sscan` module to 2.8.1 (Github tag "R2-8-1"),
  - override version of `calc` as the current `HEAD` from Github
  - deactivate the `iocStats` module altogether.
  
(Note that this kind of configuration is not guaranteed to work; in fact,
it will probably fail while attempting to build `areaDetector`, as it
depends on `iocStats`. This just serves as a way of demonstrating how
to customize the build.)

The call:

```
   # make sure the target source folder exists
   mkdir -p /home/user
   cd /home/user
   
   # ...download change whatever you like the EPICS base package
   git clone https://github.com/epics-base/epics-base my-epics
   
   # ... edit /home/user/my-epics
   
   # obtain a copy of this Dockerfile
   git clone https://gitlab.com/codedump2/epics-toolbx
   
   # build the customized image
   podman build -t epics-custom-toolbx \
                -f ./epics-toolbx/Dockerfile \
				-v /home/user/my-epics:/tmp/epics-base:z \
				-e AD_EPICS_SSCAN_VERSION="R2-8-1" \
				-e AD_EPICS_CALC_VERSION="HEAD" \
				-e AD_EPICS_IOCSTATS_VERSION=""
```

Then you can go on and enjoy your toolbox based on a customized image:
```
   toolbox create epics-custom --image epics-custom-toolbx
   toolbox enter epics-custom
   ...
```

Bugs & Contributions
--------------------

Feel free to suggest, but mostly remember: this is a "it-works-for-me"
version and package, written and supported as a ride-along, mostly in
my spare time. I welcome ideas, but I welcome patches, fixes and code
that I can integrate directly even more :-)

The code and documentation I'm writing is protected by the
[GNU General Public License](./LICENSE.md), which essentially says
that you're free to use and modify it, as long as you pass the souces
to those modifications along to your users in the same spirit.

No, it doesn't have any bugs, what are you thinking /s ... -_-
